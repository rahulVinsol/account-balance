enum AccountError: Error{
	case negativeAmountError
}

class Customer {
	private(set) static var accountCount: UInt64 = 0 
	
	let name: String
	let accountNumber: UInt64
	private(set) var balance: Double
	
	init(name: String) {
		Customer.accountCount += 1
		
		self.name = name
		self.accountNumber = Customer.accountCount
		self.balance = 1000
	}
	
	func deposit(amount: Double) throws {
		if amount < 0 {
			throw AccountError.negativeAmountError
		}
		self.balance += amount
	}
	
	func withdraw(amount: Double) throws -> Double {
		if amount < 0 {
			throw AccountError.negativeAmountError
		}
		let withdrawalAmount = min(amount, balance)
		self.balance -= withdrawalAmount
		return withdrawalAmount
	}
}

let customer = Customer(name: "Barry Allen")
print("Account Number = \(customer.accountNumber)")

do { 
	try customer.deposit(amount: 1000000)
	print("Current Balance = ", customer.balance)

	print("Amount that (can be)/(is) withdrawn = ", try customer.withdraw(amount: 1000000000))
	print("Current Balance = ", customer.balance)
} catch AccountError.negativeAmountError {
	print("Please enter non negative amount")
} catch {
	print("Unknown error occured")
}
